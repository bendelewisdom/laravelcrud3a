<form method="POST" action="/modifiche/{{ $prodotto->id }}">
    @csrf
    @method('POST')

    <!-- Nome Prodotto -->
    <label for="nome_prodotto">Nome Prodotto</label>
    <input type="text" id="nome_prodotto" name="nome_prodotto" value="{{ $prodotto->nome_prodotto }}">

    <!-- Marca -->
    <label for="marca">Marca</label>
    <input type="text" id="marca" name="marca" value="{{ $prodotto->marca }}">

    <!-- Descrizione -->
    <label for="descrizione">Descrizione</label>
    <textarea id="descrizione" name="descrizione">{{ $prodotto->descrizione }}</textarea>

    <!-- Prezzo -->
    <label for="prezzo">Prezzo</label>
    <input type="text" id="prezzo" name="prezzo" value="{{ $prodotto->prezzo }}">

    <!-- Codice Prodotto -->
    <label for="codice_prodotto">Codice Prodotto</label>
    <input type="text" id="codice_prodotto" name="codice_prodotto" value="{{ $prodotto->codice_prodotto }}">

    <label for="id">ID</label>
    <input type="hidden" name="id">

        <button type="submit">Salva Modifiche</button>

</form>

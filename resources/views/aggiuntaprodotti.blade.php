<form method="POST" action="/aggiuntaprodotti">
    @csrf
    {{-- <input type="text" name="nome_prodotto" placeholder="Nome Prodotto">
    <input type="text" name="marca" placeholder="Marca">
    <input type="text" name="descrizione" placeholder="Descrizione">
    <input type="text" name="prezzo" placeholder="Prezzo">
    <input type="text" name="codice_prodotto" placeholder="Codice_prodotto"> --}}

     <!-- Input per il nome del prodotto -->
     <label for="nome_prodotto">Nome Prodotto:</label>
     <input type="text" id="nome_prodotto" name="nome_prodotto">
 
     <!-- Input per la marca del prodotto -->
     <label for="marca">Marca:</label>
     <input type="text" id="marca" name="marca">
 
     <!-- Input per la descrizione del prodotto -->
     <label for="descrizione">Descrizione:</label>
     <textarea id="descrizione" name="descrizione"></textarea>
 
     <!-- Input per il prezzo del prodotto -->
     <label for="prezzo">Prezzo:</label>
     <input type="number" id="prezzo" name="prezzo">
 
     <!-- Input per il codice del prodotto -->
     <label for="codice_prodotto">Codice Prodotto:</label>
     <input type="text" id="codice_prodotto" name="codice_prodotto">
 
     <!-- Input per l'ID del prodotto (se necessario) -->
     <input type="hidden" name="id">
    
    <button type="submit">Aggiungi Prodotto</button>
</form>
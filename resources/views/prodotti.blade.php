<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prodotti</title>
</head>
<body>
    
    <a href="aggiuntaprodotti">
    <button type="submit">Aggiungi Prodotto</button>
    </a>


    <div class="container">
        

        
        <h1>Prodotti</h1>ò
        <div class="row">
            @foreach($prodotti as $prodotto)
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="card-title">{{ $prodotto->nome_prodotto }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $prodotto->marca }}</h6>
                            <p class="card-text">{{ $prodotto->descrizione }}</p>
                            <p class="card-text">Prezzo: ${{ $prodotto->prezzo }}</p>
                            <p class="card-text">Codice prodotto: {{ $prodotto->codice_prodotto }}</p>
                            <form action="/prodotti/{{$prodotto->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                
                                <button type="submit" onclick="return confirm('Sei sicuro di voler eliminare questo prodotto?')">Elimina</button>
                            </form>
                            <a href="/modifiche/{{ $prodotto->id }}">
                                <button>Modifica Prodotto</button>
                            </a>
                            
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    
</body>
</html>

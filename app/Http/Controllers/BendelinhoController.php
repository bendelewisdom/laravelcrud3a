<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class BendelinhoController extends Controller
{
    public function beninhoFuncionMain() {
        
        $htmlcard = $this->stampaDieciCard();
        // $sitiWeb = $this->reindirizzaASitoRandom();
        
        return view('/bendelinhosite')
        ->with('htmlcard', $htmlcard);
        // ->with('sitiWeb', $sitiWeb);
    }


    public function stampaDieciCard() {

        $htmlcard = "";
        for ($i = 0; $i < 10; $i++) {
            $linkDoveAndare = $this->getLinkRandom();
            $htmlcard .=  '
                <div class="card" style="width: 18rem; border: 2px solid red; border-radius: 20px; padding: 10px; margin: 20px;">
                    <div class="card-body">
                        <h2 class="card-title">RANDOM SITE</h2>
                        <h4 class="card-subtitle mb-6 text-muted">Sites</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the cards content.</p>
                        <a href="' . $linkDoveAndare . '" target="_blank" class="card-link">Scopri-></a>
                    </div>
                </div>';
        }
        
        return $htmlcard;
    }

    public function getLinkRandom()
    {
        // Array di siti web
        $sitiWeb = [
            'https://getbootstrap.com/docs/4.0/components/card/',
            'https://getbootstrap.com/docs/4.0/components/card/',
            'https://getbootstrap.com/docs/4.0/components/card/',
            // Aggiungi altri siti web se necessario
        ];


        // Scegli casualmente un sito web dall'array
        $sitoRandom = $sitiWeb[array_rand($sitiWeb)];

        return $sitoRandom;
    }

public function addcard(Request $request) {

    // dd($request->card_name);

    $htmlcard = $this->stampaDieciCard();

    if(isset($request->card_name)){

        $htmlcard .= "";

    }else{
        $errorMsg = "Compilare...";
        return view('/bendelinhosite')
        ->with('errorMsg', $errorMsg)
        ->with('htmlcard', $htmlcard);
    }

}

}
?>

<?php

namespace App\Http\Controllers;

use App\Models\Prodotto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProdottoController extends Controller
{
    public function index()
    {
        $prodotti= Prodotto::all();
        return view('prodotti', ['prodotti' => $prodotti]);
        }
    

    public function elimina($id)
    {
    $prodotto = Prodotto::find($id);
    

    if ($prodotto) {
        $prodotto->delete();
        return redirect()->back()->with('success', 'Prodotto eliminato con successo.');
    } 
}

public function entra() {
    return view('aggiuntaprodotti');
}

    public function visualizzaModifiche($id)
    {
        $prodotto = Prodotto::find($id);

        if (!$prodotto) {
            // Se il prodotto non è stato trovato nel database, gestisci l'errore
            return abort(404); // Mostra una pagina 404 Not Found
        }

        return view('modifiche', compact('prodotto'));
    }

    public function edit(Request $request, $id)
    {
        $prodotto = Prodotto::find($id);

        if (!$prodotto) {          
            return abort(404);
        }
        
        $aProdotto = [
        'nome_prodotto' => $request->input('nome_prodotto'),
        'marca' => $request->input('marca'), 
        'descrizione' => $request->input('descrizione'), 
        'prezzo' => $request->input('prezzo'), 
        'codice_prodotto' => $request->input('codice_prodotto'),
        ];
        
        if(isset($prodotto->id)){
            $esitoupdate = Prodotto::where('id', $prodotto->id)
            ->update($aProdotto);
            if($esitoupdate){
                $msg = 'Prodotto '. $request->nome_prodotto . ' aggiornato con successo!';
                return redirect()->route('listaProdotti')->with('success', $msg);
            }else{
                $msgn = 'Prodotto '. $request->nome_prodotto. ' non aggiornato!';
                return redirect()->route('listaProdotti')->with('error', $msgn);
            }                 
        }
    }
    

//     if($esitoupdate){
//         $msg = 'Prodotto '. $request->nome_prodotto . ' aggiornato con successo!';
//     }else{
//         $msg = 'Prodotto '. $request->nome_prodotto. ' non aggiornato!';
//     }                 
// }
// return redirect()->route('listaProdotti')->with('success', $msg);
// }




























// public function visualizzaModifiche($id) {
//     if (is_null($id) || !isset($id) || empty($id)){
//         // Errore, scrivi log, e mandalo alla tua rotta 404 not found
//     }
//     $prodotto = Prodotto::find($id);
//     if (is_null($prodotto)) {
//         // Il prodotto con quell'id non esiste decidi cosa fare
//         $prodotto->update();
//         return redirect()->back()->with('success', 'Prodotto aggiornato con successo.');
//     } 

//     return view('modifiche')->with('prodotto', $prodotto);
// }



public function store(Request $request)
{
        // Definisci le regole di validazione
        $rules = [
                    'nome_prodotto' => 'required|string',
                    'marca' => 'required|string',
                    'descrizione' => 'required|string',
                    'prezzo' => 'required|numeric',
                    'codice_prodotto' => 'required|string',
                    'id' => 'required'
        ];
    
        // Esegui la validazione dei dati
            $prodotto = new Prodotto();
            $prodotto->nome_prodotto = $request->nome_prodotto;
            $prodotto->marca = $request->marca;
            $prodotto->descrizione = $request->descrizione;
            $prodotto->prezzo = $request->prezzo;
            $prodotto->codice_prodotto = $request->codice_prodotto;
            $prodotto->id = $request->id;

            $prodotto->save();
        // Ritorna alla vista 'prodotti' con un messaggio di successo
        return redirect()->route('listaProdotti')->with('success', 'Card aggiunta con successo!');
    }
    }
    


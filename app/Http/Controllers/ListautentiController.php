<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ListautentiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('listautenti');
    }

    public function listautenti()
    {
    $lista= Listautenti::all();
    return view('listautenti', ['listautenti' => $utenti]);
    }

}

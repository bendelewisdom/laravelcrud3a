<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prodotto extends Model
{
    public $timestamps = false;
    protected $table = 'prodotti';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accesso extends Model
{
    protected $connection = 'second_mysql';
    public $timestamps = false;
    protected $tabella = 'hoodkiller';
    protected $fillable = ['id', 'nome', 'mail', 'password', 'codice_identificativo'];
}


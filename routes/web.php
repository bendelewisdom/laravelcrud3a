<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BendelinhoController;
use App\Http\Controllers\ProdottoController;
use App\Http\Controllers\AccessoController;
use App\Http\Controllers\ListautentiController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/bendelinhosite', [BendelinhoController::class, 'beninhoFuncionMain'])->name('beninhoFuncionMain');
// Route::get('/cardstamps.bendelinhosite', [BendelinhoController::class, 'reindirizzaASitoRandom'])->name('random');
Route::post('/bendelinhosite', [BendelinhoController::class, 'addcard']);
Route::get('/prove', function () {
    return view('prove');
});

Route::get('/prodotti', [ProdottoController::class, 'index'])->name('listaProdotti');
Route::delete('/prodotti/{id}', [ProdottoController::class, 'elimina']);
Route::get('/aggiuntaprodotti', [ProdottoController::class, 'entra']);
Route::post('/aggiuntaprodotti', [ProdottoController::class, 'store']);
Route::get('/modifiche/{id}', [ProdottoController::class, 'visualizzaModifiche']);
Route::post('/modifiche/{id}', [ProdottoController::class, 'edit'])->name('update');
Route::get('/accesso', [AccessoController::class, 'visualizzaAccesso'])->name('accesso');
Route::get('/listautenti', [ListautentiController::class, 'listautenti'])->name('listautenti');
Auth::routes();




